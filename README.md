## ESP32 based redox keyboard
This is an attempt to implement a ESP32 based HID keyboard based on the REDOX design.

### Parts
to reduce the number of required IO pins on the ESP 74hc165 and 74hc595 shift registers are used.
The keyboard matrix is build using diodes to prevent ghosting. 
To scan all buttons, 595 (v-out) is set high for each row and then the output for each row is read via the 165. 

### Software
Most of the heavy lifting is done by the ESP32-BLE-Keyboard library to act as a BLE keyboard.
To reduce custom code the multiplexer library RoxMux is used to handle the 165 & 595 shift registers.

### Links:
**Printing and wiring:**

- Housing: https://www.thingiverse.com/thing:2704567
- Redox Repo: https://github.com/mattdibi/redox-keyboard

**Libraries:**
- BLE KeyBoard Library:
  https://github.com/T-vK/ESP32-BLE-Keyboard
- Multiplexer Library
  https://github.com/neroroxxx/RoxMux

**ESP32**
- PINOUT: https://randomnerdtutorials.com/esp32-pinout-reference-gpios/ 