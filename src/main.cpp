#include <Arduino.h>
#include "config.h"

#include <RoxMux.h>
#include <BleKeyboard.h>


#define KEY_IDLE 0
#define KEY_PRESS 1
#define KEY_RELEASE 2

// example mapping for a the left side of a redox keyboard without explicit mapping of any non alpha numeric characters
// this will be made configurable via a custom user interface
int keyMapings[] = {'/', '1', '2', '3', '4', '5', '#', '*',
                    '/', 'q', 'w', 'e', 'r', 't', '#', '*',
                    '/', 'a', 's', 'd', 'f', 'g', '#', '*',
                    '/', 'y', 'x', 'c', 'v', 'b', '#', '*',
                    '/', '!', '\'', '§', '$', '%', '#', '*'};

byte keys[ROWS*COLS];

Rox74HC165 <MUX_TOTAL_165> mux165;
Rox74HC595<MUX_TOTAL_596> mux595;

BleKeyboard bleKeyboard(BLE_NAME, BLE_VENDOR, BLE_INITIAL_BATTERY);;

bool isKeyPressed(uint8_t col, uint8_t row) {
    return keys[row*COLS+col] == KEY_PRESS;
}

void printKeyMatrix() {
    for (uint8_t row = 0; row < ROWS; row++) {
        Serial.println();
        Serial.print("----");
        for (uint8_t col = 0; col < COLS+1; col++) {
            Serial.print("----");
        }
        Serial.println();
        for (uint8_t col = 0; col < COLS; col++) {
            Serial.printf(" | %s ", (isKeyPressed(col, row) ? "X" : " "));
        }
    }
}

bool readKey(uint8_t col, uint8_t row, bool pressed) {
    if (pressed) {
        if (keys[row*COLS+col] != KEY_PRESS) {
            keys[row*COLS+col] = KEY_PRESS;
            return true;
        }
    } else {
        if (keys[row*COLS+col] == KEY_PRESS) {
            keys[row*COLS+col] = KEY_RELEASE;
            return true;
        } else if (keys[row*COLS+col] == KEY_RELEASE) {
            keys[row*COLS+col] = KEY_IDLE;
        }
    }
    return false;
}

bool readKeys() {
    mux595.update();
    mux165.update();
    
    bool updated = false;
    for(uint8_t col=0; col < MUX_TOTAL_596*COLS; col++) {
        mux595.writePin(col, 1);
        mux595.update();
        for(uint8_t row=0;row<MUX_TOTAL_165*ROWS; row++){
            mux165.update();
            if (readKey(col, row, !mux165.readPin(row))) {
                updated = true;
            }
        }
        mux595.writePin(col, 0);
        mux595.update();
    }
    return updated;
}

bool handleKeys() {
    bool stateChange = readKeys();

    if(bleKeyboard.isConnected()) {
        for(uint8_t row=0;row<MUX_TOTAL_165*ROWS; row++){
            for(uint8_t col=0; col < MUX_TOTAL_596*COLS; col++) {
                uint8_t keyIndex = row*COLS+col;
                switch (keys[keyIndex])
                {
                case KEY_PRESS:
                  bleKeyboard.press(keyMapings[keyIndex]);
                  break;
                case KEY_RELEASE:
                  bleKeyboard.release(keyMapings[keyIndex]);
                  break;
                default:
                  break;
                }
            }
        }
    }
    return stateChange;
}

void setupMux() {
    mux165.begin(PIN_DATA_165, PIN_LOAD_165, PIN_CLK_165);
    mux595.begin(PIN_DATA_596, PIN_LATCH_596, PIN_CLK_596);
    for(uint8_t i=0;i<8;i++){
        mux595.allOn();
    }
}

void setupKeyboard() {
    Serial.begin(115200);
    Serial.println("Starting BLE work!");
    bleKeyboard.setDelay(KEY_PRESS_DELAY);
    bleKeyboard.begin();
}

void setup(){
    Serial.begin(115200);
    setupMux();
    setupKeyboard();
}


void loop(){
    if (handleKeys()) {
        printKeyMatrix();
        Serial.println();
    }
}