#ifndef _CONFIG_H 
    #define _CONFIG_H 

    #define _VERSION            20220715

    #define BLE_NAME            "RedoxESP"
    #define BLE_VENDOR          "noncreative"
    #define BLE_INITIAL_BATTERY 100

    #define ROWS                8
    #define COLS                8

    #define MUX_TOTAL_165       1
    #define PIN_DATA_165        4 // pin 9 on 74HC165 (DATA)
    #define PIN_LOAD_165        16 // pin 1 on 74HC165 (LOAD)
    #define PIN_CLK_165         18 // pin 2 on 74HC165 (CLK))


    // pins for 74HC595 
    #define MUX_TOTAL_596       1
    #define PIN_DATA_596        22 // pin 14 on 74HC595 (DATA)
    #define PIN_LATCH_596       21  // pin 12 on 74HC595 (LATCH)
    #define PIN_CLK_596         18  // pin 11 on 74HC595 (CLK)

    #define KEY_PRESS_DELAY     8

#endif // _CONFIG_H